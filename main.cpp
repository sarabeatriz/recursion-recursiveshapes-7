
#include <QApplication>
#include <iostream>
#include <QLineEdit>
#include <QObject>
#include <QAction>
#include <QPushButton>
#include <cmath>
#include <QDebug>
#include "drawingWindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;

    w.resize(500, 500);
    w.show();


    int size = 333, level = 3;

    // Call the snowflake recursive function
    // Invoca la funcion recursiva del copo de nieve
    w.snowHelper(size,level);


    // Calls de boxes recursive funtion
    // Invocacion de la funcion recursiva para las cajas.
    // (x, y, sideLength, shrinkFactor, smallest Length, Qt::<color>)
   
    // YOUR CODE HERE

    return a.exec();

}


